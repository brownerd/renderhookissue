+++
layout = "hack"
text = "![](https://brownerd.gitlab.io/renderhookissue/keyboard.jpg)"
+++

```html
<!--
    Place {{ .Content }} at the top
    to get the RenderHook to fire on the .Params.text call.
    .Content can be empty too.
-->
{{ .Content }}
<div>{{ .Params.text | $.Page.RenderString }}</div>
```
