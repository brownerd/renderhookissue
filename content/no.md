+++
text = "![](https://brownerd.gitlab.io/renderhookissue/keyboard.jpg)"
layout = "no"
+++

```html
<div>{{ .Params.text | $.Page.RenderString }}</div>
```
