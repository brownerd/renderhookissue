# RenderHook issue

This mini site demonstrates the RenderHook issue.

### There are three pages:

- [Yes](https://brownerd.gitlab.io/renderhookissue/yes/) - RenderHook fires and image is styled with a Red border
- [No](https://brownerd.gitlab.io/renderhookissue/no/) - RenderHook does NOT fire. Image is NOT styled with a Red border.
- [Hack](https://brownerd.gitlab.io/renderhookissue/hack/) - This page uses a simple hack to force the RenderHook to fire, and style the image with a Red border

### Conclusion

1. RenderHooks do work on Markdown within the `{{ .Content }}` page Variable.
2. RenderHools do NOT work on a Frontmatter value containing a Markdown String.
3. You can force the Frontmatter values to render by placing the `{{ .Content }}` page variable above the call to `{{ .Params.text | $.Page.RenderString }}`.
